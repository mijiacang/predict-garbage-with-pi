#-*- coding:utf-8 -*-

'''
  Copyright (c) [2021] [CaliFall]
   [GarbageDetectGolf] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.  
'''

import cv2
 
cap=cv2.VideoCapture(0)
i=1
while(1):
    ret ,frame = cap.read()
    k=cv2.waitKey(1)
    if k==27:
        break
    elif k==32:
        cv2.imwrite('/home/pi/weightphotos_?/?/?_1_'+str(i)+'.jpg',frame)
        print("OK!"+"||"+str(i))
        i+=1
    cv2.imshow("capture", frame)
cap.release()
cv2.destroyAllWindows()
